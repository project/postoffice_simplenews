Contents
--------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * See Also


Introduction
------------

The [Postoffice](https://www.drupal.org/project/postoffice) module sends themed
emails with [Symfony Mailer](https://symfony.com/doc/current/mailer.html).

This module provides a mail plugin for simplenews compatibility.

 * For a full description of the module, visit the
   [project page](https://www.drupal.org/project/postoffice_simplenews)
 * To submit bug reports and feature suggestions, or track changes, visit the
   [issue tracker](https://www.drupal.org/project/issues/postoffice_simplenews)


Requirements
------------

This module requires [Postoffice](https://www.drupal.org/project/postoffice) and
[Simplenews](https://www.drupal.org/project/simplenews)


Installation
------------

 * Install as you would normally install a contributed Drupal module. Visit the
   [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
   documentation page for further information.


Configuration
-------------

 * Setup `postoffice_simplenews_mail` as the mail plugin for mails sent by simplenews:
   ```
   drush config:set system.mail interface.simplenews postoffice_simplenews_mail
   ```


See Also
--------

This module is geared towards developers. If it does not suit your modus
operandi, please have a look at the
[Symfony Mailer](https://www.drupal.org/project/symfony_mailer) module.


Maintainers
-----------

Current maintainers:

 * [znerol](https://www.drupal.org/u/znerol)
