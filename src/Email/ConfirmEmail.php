<?php

namespace Drupal\postoffice_simplenews\Email;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\postoffice\Email\LocalizedEmailInterface;
use Drupal\postoffice\Email\SiteEmailInterface;
use Drupal\postoffice\Email\SiteEmailTrait;
use Drupal\postoffice\Email\TemplateAttachmentsInterface;
use Drupal\postoffice\Email\TemplateAttachmentsTrait;
use Drupal\postoffice\Email\ThemedEmailInterface;
use Drupal\postoffice\Email\UrlOptionsTrait;
use Drupal\postoffice_compat\Email\CompatEmailTrait;
use Drupal\simplenews\SubscriberInterface;
use Symfony\Component\Mime\Email;

/**
 * Postoffice email for combined subscriber/unsubscribe confirmation messages generated by Simplenews.
 */
class ConfirmEmail extends Email implements ConfirmEmailInterface, LocalizedEmailInterface, SiteEmailInterface, TemplateAttachmentsInterface, ThemedEmailInterface {

  use CompatEmailTrait;
  use SiteEmailTrait;
  use SubscriberRouteParamsTrait;
  use TemplateAttachmentsTrait;
  use UrlOptionsTrait;

  /**
   * Subscriber entity.
   */
  protected SubscriberInterface $subscriber;

  /**
   * Constructs a new email for messages generated by Simplenews.
   */
  public function __construct(
    SubscriberInterface $subscriber,
    ?ImmutableConfig $siteConfig = NULL,
    ?LanguageManagerInterface $languageManager = NULL
  ) {
    parent::__construct();
    $this->subscriber = $subscriber;
    $this->siteConfig = $siteConfig;
    $this->languageManager = $languageManager;
  }

  /**
   * Constructs a new simplenews email from a core message structure.
   */
  public static function createFromMessage(array $message): static {
    return (new static($message['params']['context']['simplenews_subscriber']))
      ->headersFromMessage($message)
      ->subject($message['subject'])
      ->to($message['to'])
      ->html($message['body']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildThemedEmail(): ?array {
    return [
      '#theme' => 'postoffice_simplenews_confirm_email',
      '#body' => [
        '#markup' => Markup::create($this->getHtmlBody()),
      ],
      '#email' => $this,
      '#langcode' => $this->getLangcode(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(): string {
    return $this->subscriber->getLangcode();
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscriber(): SubscriberInterface {
    return $this->subscriber;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmUrl(): string {
    $action = 'combined' . serialize($this->subscriber->getChanges());
    $params = $this->generateSubscriberRouteParams($action);
    return Url::fromRoute('simplenews.newsletter_confirm_combined', $params, $this->getUrlOptions())->toString();
  }

}
