<?php

namespace Drupal\postoffice_simplenews\Email;

use Drupal\simplenews\SubscriberInterface;

/**
 * Interface for simplenews subscriber email.
 */
interface ConfirmEmailInterface {

  /**
   * Return the subscriber entity.
   */
  public function getSubscriber(): SubscriberInterface;

  /**
   * Returns the combined subscribe/unsubscribe confirmation URL.
   *
   * Accessible via email.confirmUrl from twig templates.
   */
  public function getConfirmUrl(): string;

}
