<?php

namespace Drupal\postoffice_simplenews\Email;

use Drupal\simplenews\SubscriberInterface;

/**
 * Interface for simplenews subscriber email.
 */
interface IssueEmailInterface {

  /**
   * Returns the newsletter id.
   */
  public function getNewsletterId(): string;

  /**
   * Return the subscriber entity.
   */
  public function getSubscriber(): SubscriberInterface;

  /**
   * Returns an unsubscribe url for the newsletter.
   *
   * Accessible via email.unsubscribeUrl from twig templates.
   */
  public function getUnsubscribeUrl(): string;

  /**
   * Returns TRUE if this is a test email.
   *
   * Accessible via email.test from twig templates.
   */
  public function isTest(): bool;

}
