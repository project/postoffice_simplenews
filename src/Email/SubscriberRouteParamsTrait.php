<?php

namespace Drupal\postoffice_simplenews\Email;

use Drupal\Component\Datetime\TimeInterface;

trait SubscriberRouteParamsTrait {

  /**
   * The clock service.
   */
  protected ?TimeInterface $time;

  /**
   * Generate route params for the given action.
   */
  protected function generateSubscriberRouteParams(string $action, array $additionalParams = []): array {
    $time = $this->getTime();
    $subscriber = $this->getSubscriber();
    $hash = simplenews_generate_hash($subscriber->getMail(), $action, $time->getRequestTime());

    return [
      'snid' => $subscriber->id(),
      'timestamp' => $time->getRequestTime(),
      'hash' => $hash,
    ] + $additionalParams;
  }

  /**
   * Returns the clock.
   */
  protected function getTime(): TimeInterface {
    return $this->time ?? \Drupal::time();
  }

}
