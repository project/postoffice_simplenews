<?php

namespace Drupal\postoffice_simplenews\Email;

use Drupal\simplenews\SubscriberInterface;

/**
 * Interface for simplenews subscriber email.
 */
interface SubscriptionSettingsEmailInterface {

  /**
   * Return the subscriber entity.
   */
  public function getSubscriber(): SubscriberInterface;

  /**
   * Returns an url to the subscription settings page.
   *
   * Accessible via email.subscriptionSettingsUrl from twig templates.
   */
  public function getSubscriptionSettingsUrl(): string;

}
