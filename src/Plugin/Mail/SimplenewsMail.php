<?php

namespace Drupal\postoffice_simplenews\Plugin\Mail;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\postoffice_compat\Plugin\Mail\CompatMailBase;
use Drupal\postoffice_simplenews\Email\ConfirmEmail;
use Drupal\postoffice_simplenews\Email\IssueEmail;
use Drupal\postoffice_simplenews\Email\SubscriptionSettingsEmail;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Defines a mail backend Simplenews, using Symfony Mailer via Postoffice.
 *
 * @Mail(
 *   id = "postoffice_simplenews_mail",
 *   label = @Translation("Postoffice Simplenews Mail"),
 *   description = @Translation("Sends Simplenews mail messages using Symfony Mailer via Postoffice.")
 * )
 */
class SimplenewsMail extends CompatMailBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('postoffice.mailer'),
      $container->get('logger.channel.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function emailFromMessage(array $message): RawMessage {
    switch ($message['id']) {
      case 'simplenews_test':
      case 'simplenews_node':
        return IssueEmail::createFromMessage($message);

      case 'simplenews_subscribe_combined':
        return ConfirmEmail::createFromMessage($message);

      case 'simplenews_validate':
        return SubscriptionSettingsEmail::createFromMessage($message);

      default:
        throw new \InvalidArgumentException(static::class . ' cannot handle message with id ' . $message['id']);
    }
  }

}
