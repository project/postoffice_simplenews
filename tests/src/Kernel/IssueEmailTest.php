<?php

namespace Drupal\Tests\postoffice_simplenews\Kernel;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\node\Entity\Node;
use Drupal\postoffice_simplenews\Email\IssueEmail;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\Mail\MailCacheNone;
use Drupal\simplenews\Mail\MailEntity;
use Drupal\Tests\postoffice_compat\Kernel\CompatTestBase;

/**
 * Tests for IssueEmail.
 *
 * @group postoffice_simplenews
 */
class IssueEmailTest extends CompatTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    /* see EntityKernelTestBase */
    'user',
    'system',
    'field',
    'text',
    'filter',

    /* primary dependency */
    'node',
    'simplenews',
    'views',

    /* system under test */
    'postoffice_simplenews',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    DateFormat::create([
      'id' => 'fallback',
      'label' => 'Fallback date format',
      'langcode' => 'en',
      'locked' => TRUE,
      'pattern' => 'D, m/d/Y - H:i',
      'status' => TRUE,
    ])->save();

    /* see EntityKernelTestBase */
    $this->installConfig(['user']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');

    $this->installConfig(['node']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('node');

    $this->installEntitySchema('view');

    $this->installConfig(['field']);

    /* see simplenews.install */
    $this->installConfig(['simplenews']);
    $this->installSchema('simplenews', ['simplenews_mail_spool']);
    $this->installEntitySchema('simplenews_subscriber');
  }

  /**
   * Verify that confirm url is accessible via twig.
   */
  public function testIssueEmailTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')->install(['stark', 'postoffice_simplenews_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_simplenews_test_theme')->save();

    $subscriber = Subscriber::create(
      ['mail' => $this->randomMachineName() . '@example.com']
    );
    $subscriber->save();

    // Create a very basic node.
    $node = Node::create([
      'type' => 'simplenews_issue',
      'title' => $this->randomMachineName(),
      'uid' => 0,
      'status' => 1,
      'body' => $this->randomMachineName(),
    ]);
    $node->simplenews_issue->target_id = 'default';
    $node->simplenews_issue->handler = 'simplenews_all';
    $node->save();

    $mail = new MailEntity($node, $subscriber, new MailCacheNone());
    $mail->setKey('node');

    $coreMessage = $this->createCoreMessage('simplenews', 'node');
    $coreMessage['params']['simplenews_mail'] = $mail;

    $email = IssueEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertMatchesRegularExpression('@unsubscribeUrl=https?://[^/]*/newsletter/confirm/.+/.+/.+/.{20,}@', $actual);
    $this->assertStringContainsString('test=NO', $actual);
  }

  /**
   * Verify that confirm url is accessible via twig.
   */
  public function testTestIssueEmailTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')->install(['stark', 'postoffice_simplenews_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_simplenews_test_theme')->save();

    $subscriber = Subscriber::create(
      ['mail' => $this->randomMachineName() . '@example.com']
    );
    $subscriber->save();

    // Create a very basic node.
    $node = Node::create([
      'type' => 'simplenews_issue',
      'title' => $this->randomMachineName(),
      'uid' => 0,
      'status' => 1,
      'body' => $this->randomMachineName(),
    ]);
    $node->simplenews_issue->target_id = 'default';
    $node->simplenews_issue->handler = 'simplenews_all';
    $node->save();

    $mail = new MailEntity($node, $subscriber, new MailCacheNone());
    $mail->setKey('test');

    $coreMessage = $this->createCoreMessage('simplenews', 'test');
    $coreMessage['params']['simplenews_mail'] = $mail;

    $email = IssueEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertStringContainsString('unsubscribeUrl=#', $actual);
    $this->assertStringContainsString('test=YES', $actual);
  }

}
