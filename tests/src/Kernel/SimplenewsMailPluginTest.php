<?php

namespace Drupal\Tests\postoffice_simplenews\Kernel;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Form\FormState;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\simplenews\Entity\Newsletter;
use Drupal\simplenews\Form\SubscriberValidateForm;
use Drupal\Tests\postoffice_compat\Kernel\CompatTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\Role;
use Symfony\Component\Mime\Address;

/**
 * Tests the sending of order receipt emails.
 *
 * @group postoffice_simplenews
 */
class SimplenewsMailPluginTest extends CompatTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    /* see EntityKernelTestBase */
    'user',
    'system',
    'field',
    'text',
    'filter',

    /* primary dependency */
    'node',
    'simplenews',
    'views',

    /* system under test */
    'postoffice_simplenews',
  ];

  /**
   * The address used as from / reply-to address for sitewide simplenews mails.
   */
  protected Address $simplenewsFrom;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    DateFormat::create([
      'id' => 'fallback',
      'label' => 'Fallback date format',
      'langcode' => 'en',
      'locked' => TRUE,
      'pattern' => 'D, m/d/Y - H:i',
      'status' => TRUE,
    ])->save();

    /* see EntityKernelTestBase */
    $this->installConfig(['user']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');

    $this->installConfig(['node']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('node');

    $this->installEntitySchema('view');

    $this->installConfig(['field']);

    /* see simplenews.install */
    $this->installConfig(['simplenews']);
    $this->installSchema('simplenews', ['simplenews_mail_spool']);
    $this->installEntitySchema('simplenews_subscriber');

    Role::load(AccountInterface::ANONYMOUS_ROLE)
      ->grantPermission('subscribe to newsletters')
      ->save();
    Role::load(AccountInterface::AUTHENTICATED_ROLE)
      ->grantPermission('subscribe to newsletters')
      ->save();

    // Init the default newsletter.
    $newsletter = Newsletter::load('default');
    $newsletter->from_name = $this->randomMachineName();
    $newsletter->from_address = $this->randomMachineName() . '@example.com';
    $newsletter->format = 'html';
    $newsletter->save();

    $this->simplenewsFrom = new Address($this->randomMachineName() . '@example.com', $this->randomMachineName());
    $this->config('simplenews.settings')
      ->set('newsletter.from_name', $this->simplenewsFrom->getName())
      ->set('newsletter.from_address', $this->simplenewsFrom->getAddress())
      ->set('mail.textalt', TRUE)
      ->save();

    $this->config('system.mail')
      ->set('interface.simplenews', 'postoffice_simplenews_mail')
      ->save();
  }

  public function testSend() {
    // Create a very basic node.
    $node = Node::create([
      'type' => 'simplenews_issue',
      'title' => $this->randomMachineName(),
      'uid' => 0,
      'status' => 1,
      'body' => $this->randomMachineName(),
    ]);
    $node->simplenews_issue->target_id = 'default';
    $node->simplenews_issue->handler = 'simplenews_all';
    $node->save();

    /** @var \Drupal\simplenews\Subscription\SubscriptionManagerInterface */
    $subscriptionManager = $this->container->get('simplenews.subscription_manager');

    $subscriberEmail = $this->randomMachineName() . '@example.com';
    $subscriptionManager->subscribe($subscriberEmail, 'default', FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($node) {
      /** @see \Drupal\Test\simplenews\Functional\SimplenewsSendTest::testProgrammaticNewsletter */
      $this->container->get('simplenews.spool_storage')->addIssue($node);
      $this->container->get('simplenews.mailer')->sendSpool();
      $this->container->get('simplenews.spool_storage')->clear();
      $this->container->get('simplenews.mailer')->updateSendStatus();
    });

    $this->assertCount(1, $recordedEmails);

    /** @var \Drupal\postoffice_simplenews\Email\ConfirmEmail $email */
    [$email] = $recordedEmails;

    $newsletter = Newsletter::load('default');
    $newsletterFrom = new Address($newsletter->from_address, $newsletter->from_name);
    $this->assertEquals($newsletterFrom->toString(), $email->getFrom()[0]->toString());
    $this->assertEquals($newsletterFrom->toString(), $email->getReplyTo()[0]->toString());
    $this->assertEquals($subscriberEmail, $email->getTo()[0]->toString());
    $this->assertNotEmpty($node->getTitle());
    $this->assertStringContainsString($node->getTitle(), $email->getSubject());
    $this->assertMatchesRegularExpression('@newsletter/confirm/.+/.+/.+/.{20,}@', $email->getHeaders()->get('List-Unsubscribe')->getBodyAsString());

    $this->assertNotEmpty($node->body->value);
    $this->assertStringContainsString($node->body->value, $email->getHtmlBody());
    $this->assertMatchesRegularExpression('@newsletter/confirm/.+/.+/.+/.{20,}@', $email->getHtmlBody());
    $this->assertStringContainsString($node->body->value, $email->getTextBody());
    $this->assertMatchesRegularExpression('@newsletter/confirm/.+/.+/.+/.{20,}@', $email->getTextBody());
  }

  public function testConfirm() {
    /** @var \Drupal\simplenews\Subscription\SubscriptionManagerInterface */
    $subscriptionManager = $this->container->get('simplenews.subscription_manager');
    $subscriberEmail = $this->randomMachineName() . '@example.com';

    $recordedEmails = $this->callAndRecordEmails(function () use ($subscriptionManager, $subscriberEmail) {
      $subscriptionManager->subscribe($subscriberEmail, 'default', TRUE);
      $sent = $subscriptionManager->sendConfirmations();
      $this->assertEquals(1, $sent);
    });

    $this->assertCount(1, $recordedEmails);

    /** @var \Drupal\postoffice_simplenews\Email\ConfirmEmail $email */
    [$email] = $recordedEmails;
    $this->assertEquals($this->simplenewsFrom->toString(), $email->getFrom()[0]->toString());
    $this->assertEquals($this->simplenewsFrom->getAddress(), $email->getReplyTo()[0]->toString());
    $this->assertEquals($subscriberEmail, $email->getTo()[0]->toString());
    $this->assertMatchesRegularExpression('@newsletter/confirm/.+/.+/.+/.{20,}@', $email->getHtmlBody());
  }

  public function testSubscriptionSettings() {
    /** @var \Drupal\simplenews\Subscription\SubscriptionManagerInterface */
    $subscriptionManager = $this->container->get('simplenews.subscription_manager');

    /** @var \Drupal\Core\Form\FormBuilderInterface */
    $formBuilder = $this->container->get('form_builder');
    $subscriberEmail = $this->randomMachineName() . '@example.com';

    $subscriptionManager->subscribe($subscriberEmail, 'default', FALSE);
    $recordedEmails = $this->callAndRecordEmails(function () use ($formBuilder, $subscriberEmail) {
      $formState = new FormState();
      $formState->setValue('mail', $subscriberEmail);
      $formBuilder->submitForm(SubscriberValidateForm::class, $formState);
      $this->assertCount(0, $formState->getErrors());
    });

    $this->assertCount(1, $recordedEmails);

    /** @var \Drupal\postoffice_simplenews\Email\ConfirmEmail $email */
    [$email] = $recordedEmails;
    $this->assertEquals($this->simplenewsFrom->toString(), $email->getFrom()[0]->toString());
    $this->assertEquals($this->simplenewsFrom->getAddress(), $email->getReplyTo()[0]->toString());
    $this->assertEquals($subscriberEmail, $email->getTo()[0]->toString());
    $this->assertMatchesRegularExpression('@newsletter/subscriptions/.+/.+/.{20,}@', $email->getHtmlBody());
  }

}
