<?php

namespace Drupal\Tests\postoffice_simplenews\Kernel;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\postoffice_simplenews\Email\SubscriptionSettingsEmail;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\Tests\postoffice_compat\Kernel\CompatTestBase;

/**
 * Tests for SubscriptionSettingsEmail.
 *
 * @group postoffice_simplenews
 */
class SubscriptionSettingsEmailTest extends CompatTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    /* see EntityKernelTestBase */
    'user',
    'system',
    'field',
    'text',
    'filter',

    /* primary dependency */
    'node',
    'simplenews',
    'views',

    /* system under test */
    'postoffice_simplenews',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    DateFormat::create([
      'id' => 'fallback',
      'label' => 'Fallback date format',
      'langcode' => 'en',
      'locked' => TRUE,
      'pattern' => 'D, m/d/Y - H:i',
      'status' => TRUE,
    ])->save();

    /* see EntityKernelTestBase */
    $this->installConfig(['user']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');

    $this->installConfig(['node']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('node');

    $this->installEntitySchema('view');

    $this->installConfig(['field']);

    /* see simplenews.install */
    $this->installConfig(['simplenews']);
    $this->installSchema('simplenews', ['simplenews_mail_spool']);
    $this->installEntitySchema('simplenews_subscriber');
  }

  /**
   * Verify that validate url is accessible via twig.
   */
  public function testSubscriptionSettingsEmailTwigVariables() {
    // Setup custom theme.
    $this->container->get('theme_installer')->install(['stark', 'postoffice_simplenews_test_theme']);
    $this->config('system.theme')->set('default', 'postoffice_simplenews_test_theme')->save();

    $subscriber = Subscriber::create(
      ['mail' => $this->randomMachineName() . '@example.com']
    );
    $subscriber->save();

    $coreMessage = $this->createCoreMessage('simplenews', 'subscribe_combined');
    $coreMessage['params']['context']['simplenews_subscriber'] = $subscriber;

    $email = SubscriptionSettingsEmail::createFromMessage($coreMessage);
    $recordedEmails = $this->callAndRecordEmails(function () use ($email) {
      $this->container->get('postoffice.mailer')->send($email);
    });

    $this->assertCount(1, $recordedEmails);
    $actual = trim($recordedEmails[0]->getHtmlBody());
    $this->assertMatchesRegularExpression('@newsletter/subscriptions/.+/.+/.{20,}@', $actual);
  }

}
